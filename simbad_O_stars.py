from astroquery.simbad import Simbad
import sys
import csv
from pprint import pprint

stars = [
"HD 93129A",
"SS 215",
"WR 21a",
"HD 93162",
"Schulte 22A",
"Schulte 7",
"HD 64568",
"Cl Trumpler 16 244",
"2MASS J20555125+4352246",
"HD 93128",
"HD 93129B",
"V* V560 Car",
"LS III +46 11",
"NGC 3603 18",
"HD 319718",
"TYC 8958-2723-1",
"TYC 4279-1463-1",
"GSC 08958-04287",
"HD 150136",
"* 9 Sgr",
"NGC 6618 337",
"BD+43 3654",
"BD+50 886",
"BD-13 4923",
"HD 15570",
"HD 16691",
"HD 168076",
"HD 190429A",
"HD 46223",
"HD 5005A",
"HD 93250",
"HD 96715",
"HD 228766",
"HD 229232",
"Cl* NGC 3603 SHER 47",
"* zet Pup",
"LS III +41 20",
"RL 128",
"CD-38 11748",
"Schulte 8C",
"Schulte 9",
"HD 14947",
"HD 15558",
"HD 15629",
"HD 155913",
"V* V2011 Cyg",
"HD 193682",
"HD 242908",
"HD 303308",
"LS III +46 12",
"LS III +57 18",
"Cl* HM 1 VB 10",
"TYC 8958-4232-1",
"BD+45 3216A",
"CD-47 4551",
"HD 14442",
"HD 168112",
"HD 46150",
"HD 93632",
"HD 93843",
"HD 97253",
"HD 256725",
"HD 319699",
"Cl* NGC 3603 MTT 25",
"TYC 7383-421-1",
"V* V662 Car",
"ALS 17591",
"V* V441 Pup",
"LS III +56 109",
"GSC 07870-00795",
"2MASS J17253421-3423116",
"BD+60 134",
"BD-14 5040",
"BD-16 4826",
"CPD-59 2673",
"Schulte 28",
"HD 215835",
"V* V1051 Cen",
"HD 14434",
"HD 48099",
"HD 64315",
"HD 93204",
"HD 93403",
"HD 305525",
"V* MY Cam",
"Cl* NGC 3603 MDS 51",
"V* V747 Cep",
"Cl* HM 1 VB 20",
"2MASS J17252916-3425157",
"LS 3386",
"LS IV -12 12",
"BD+60 2635",
"CD-58 3526",
"CD-59 3300",
"CD-59 3310",
"Schulte 22B",
"BD+40 4227A",
"BD+40 4227B",
"[CPR2002] B17",
"HD 101190",
"HD 124314A",
"HD 148937",
"HD 152233",
"HD 153919",
"HD 165052",
"HD 169582",
"HD 194649",
"IC 1396",
"HD 39680",
"HD 42088",
"HD 76556",
"HD 92206A",
"HD 92206B",
"V* V382 Cyg",
"HD 229196",
"HD 303311",
"VdBH 86a",
"HD 338931",
"2MASS J17244578-3409399",
"TYC 7370-460-1",
"Cl* Trumpler 16 MJ 568",
"HD 191612",
"HD 152386",
"Hilt 1118",
"UCAC2 44720820",
"TYC 5968-4011-1",
"TYC 5987-2050-1",
"BD+60 2522",
"BD+60 497",
"BD+61 411",
"BD+62 424",
"BD-15 1909",
"CD-26 5136",
"CD-28 5104",
"[CPR2002] A24",
"HD 101298",
"HD 101436",
"HD 12993",
"HD 130298",
"HD 150135",
"HD 150958A",
"HD 152723",
"HD 156738",
"HD 157857",
"HD 163758",
"HD 167633",
"HD 168075",
"HD 17505A",
"HD 172175",
"HD 175876",
"HD 18326",
"HD 190864",
"HD 199579",
"HD 91572",
"V* V661 Car",
"HD 93161B",
"HD 96946",
"HD 99897",
"HD 227018",
"HD 228759",
"HD 228841",
"HD 242935A",
"HD 305524",
"HD 305532",
"HD 322417",
"HD 326775",
"HD 344784",
"V* V467 Vel",
"* lam Cep",
"HD 108",
"GSC 03157-00531",
"BD+40 4220",
"* 15 Mon",
"* 29 CMa",
"GSC 08612-00589",
"LS III +55 45",
"LS III +57 90",
"TYC 3161-1048-1",
"Cl* NGC 6611 BKP 29598",
"Cl* HM 1 VB 24",
"Hilt 63",
"LS V +38 12",
"LS V +33 15",
"LS III +55 37",
"BD+60 501",
"BD+60 513",
"BD+60 586A",
"BD+62 2078",
"BD-04 4503",
"BD-10 4682",
"BD-13 4927",
"CPD-26 2704",
"CD-47 4550",
"CD-58 3529",
"BD+40 4219",
"[CPR2002] A11",
"HD 101205",
"HD 151515",
"HD 152248",
"HD 152623",
"V* V1036 Sco",
"HD 165921",
"HD 167659",
"HD 167771",
"V* V1182 Aql",
"HD 193514",
"HD 193595",
"HD 217086A",
"HD 36879",
"HD 44811",
"HD 46485",
"HD 46573",
"HD 54662",
"HD 5689",
"HD 69464",
"HD 91824",
"HD 93146A",
"HD 93160",
"HD 93222",
"HD 94370A",
"HD 94963",
"HD 97966",
"HD 227245",
"HD 227465",
"HD 242926",
"HD 303316",
"HD 319703",
"ALS 18660",
"TYC 737-1170-1",
"NGC 7822 29",
"* del Cir",
"* tet01 Ori C",
"HD 313846",
"NAME Her 36",
"2MASS J20315961+4114504",
"* 68 Cyg",
"* 9 Sge",
"GSC 03161-01365",
"Cl* Trumpler 14 MJ 92",
"LS 85",
"BD+33 1025",
"BD+55 2840",
"BD+60 261",
"BD+66 1675",
"BD-14 5014",
"CD-34 4496",
"CD-49 4263",
"CPD-59 2626",
"CPD-61 3973",
"Schulte 16",
"TYC 3161-1195-1",
"V* EM Car",
"HD 117797",
"HD 120521",
"HD 124979",
"V* V1297 Sco",
"HD 155806",
"HD 156154",
"HD 163800",
"HD 164492A",
"HD 164536",
"HD 166734",
"HD 168461",
"HD 168504",
"HD 17603",
"HD 171589",
"HD 186980",
"HD 192639",
"HD 213023A",
"HD 34656",
"HD 35619",
"HD 41997",
"HD 53975",
"HD 74920",
"HD 93161A",
"HD 97166",
"HD 97319",
"HD 97434",
"HD 99546",
"HD 229202",
"HD 338916",
"HD 344777",
"2MASS J17242895-3414506",
"V* V572 Car",
"* ksi Per",
"2MASS J17244229-3413213",
"CD-58 3214",
"* 63 Oph",
"V* AB Cru",
"LS II +39 53",
"GSC 03161-01218",
"TYC 3161-1174-1",
"GSC 03157-00327",
"Hilt 233",
"Hilt 412",
"BD+40 4179",
"BD+52 805",
"BD+55 2722",
"BD-11 4586",
"CPD-59 2591",
"V* V731 Car",
"CPD-59 2636",
"Schulte 1",
"TYC 3161-1199-1",
"GSC 03161-01136",
"Schulte 24",
"GSC 03161-01067",
"GSC 03161-01397",
"HD 101191",
"HD 101223",
"HD 101413",
"HD 115455",
"HD 116282",
"HD 123590",
"HD 135591",
"HD 145217",
"HD 151804",
"HD 161853",
"HD 165246",
"HD 168137",
"HD 17505B",
"HD 17520",
"HD 175754",
"HD 191978",
"HD 225160",
"HD 41161",
"HD 46056",
"HD 47129",
"CD-57 3378",
"HD 93343",
"HD 94024",
"HD 95589",
"HD 96917",
"HD 97848",
"HD 305438",
"HD 305539",
"HD 305612",
"HD 319702",
"HD 326331",
"CD-45 11051",
"HD 167971",
"GSC 08701-00485",
"V* TU Mus",
"* lam Ori",
"HD 60848",
"HD 152408A"]

MySimbad = Simbad()
MySimbad.ROW_LIMIT = 1000 # now any query fetches at most 15 rows
MySimbad.TIMEOUT = 60 # sets the timeout to 20s

MySimbad.reset_votable_fields()
MySimbad.remove_votable_fields('coordinates')
MySimbad.add_votable_fields('otype', 'ra(dec,ICRS,J2000,2000)', 'dec(dec,ICRS,J2000,2000)', 'sptype', 'flux(B)', \
                            'flux(V)', 'flux(J)', 'flux(H)', 'flux(K)')
VOTfields= MySimbad.get_votable_fields()
pprint(VOTfields)

all = []
for s in stars:
    result = MySimbad.query_object(s)
    all.append(result)
csv_columns = result.keys()
csv_columns.remove('SP_QUAL')
csv_columns.remove('SP_BIBCODE')

csv_file = "Ostars.csv"
try:
    with open(csv_file, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
        writer.writeheader()
        for result in all:
            for row in result:
                dicc = {}
                for c in csv_columns:
                    dicc[c] = row[c]
                writer.writerow(dicc)
except IOError:
    print("I/O error")

