import tensorflow as tf
import numpy as np
import pandas as pd
from tensorflow import keras
from tensorflow.keras import layers
import csv
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from tensorflow.keras.layers.experimental.preprocessing import Normalization
from tensorflow.keras.layers.experimental.preprocessing import CategoryEncoding
from tensorflow.keras.layers.experimental.preprocessing import StringLookup
from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.layers import Dense
from tensorflow.keras import Input 
from tensorflow.keras.optimizers import SGD
import sys
import matplotlib.pyplot as plt
from pprint import pprint


# Samples per class
SPC = 150

dataset = []
with open('stars.csv','r') as fp:
    for sample in csv.DictReader(fp):
        dataset.append(sample)

csv_columns = dataset[0].keys()
classes = {"O": 1, "B": 2, "A": 3, "F": 4, "G": 5, "K": 6, "M": 7}
counters = {"O": 0, "B": 0, "A": 0, "F": 0, "G": 0, "K": 0, "M": 0}

X = []
y = []
training_set = []
for s in dataset:
    try:
        if counters[s["CLASS"]] <= SPC and s["FLUX_B"] != "" and s["FLUX_V"] != "" and s["FLUX_J"] != "" and s["FLUX_H"] != "" and s["FLUX_K"] != "":
            new_s = {}
            new_s['f1'] = float(s["FLUX_B"])
            new_s['f2'] = float(s["FLUX_V"])
            new_s['f3'] = float(s["FLUX_J"])
            new_s['f4'] = float(s["FLUX_H"])
            new_s['f5'] = float(s["FLUX_K"])
            features_lst = []
            for feature in new_s:
                features_lst.append(new_s[feature])
            X.append(features_lst)
            y.append(classes[s["CLASS"]])
            counters[s["CLASS"]] += 1
            training_set.append(s)
    except Exception as e:
        #print("Error in sample: " + s["MAIN_ID"])
        continue

X = np.array(X)
y = np.array(y)
X = StandardScaler().fit_transform(X)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.4, random_state=0)

# convert the labels from integers to vectors (for 2-class, binary
# classification you should use Keras' to_categorical function
# instead as the scikit-learn's LabelBinarizer will not return a
# vector)
lb = LabelBinarizer()
y_train = lb.fit_transform(y_train)
y_test = lb.transform(y_test)

model = Sequential()
model.add(Input(shape=(5,)))
model.add(Dense(40, activation="relu"))
model.add(Dense(20, activation="relu"))
model.add(Dense(10, activation="relu"))
model.add(Dense(len(lb.classes_), activation="softmax"))
model.summary()
"""
inputs = Input(shape=(5,))
x = Dense(40, activation="relu")(inputs)
x = Dense(20, activation="relu")(x)
x = Dense(10, activation="relu")(x)
outputs = tf.keras.layers.Dense(len(lb.classes_), activation="softmax")(x)
model = Model(inputs=inputs, outputs=outputs)
"""
# initialize our initial learning rate and # of epochs to train for
INIT_LR = 0.01
EPOCHS = 200
# compile the model using SGD as our optimizer and categorical
# cross-entropy loss (you'll want to use binary_crossentropy
# for 2-class classification)
print("[INFO] training network...")
opt = SGD(lr=INIT_LR)
model.compile(loss="categorical_crossentropy", optimizer=opt,
	metrics=["accuracy"])
keras.utils.plot_model(model, show_shapes=True, rankdir="LR")
# train the neural network
H = model.fit(x=X_train, y=y_train, validation_data=(X_test, y_test),
	epochs=EPOCHS, batch_size=32)
# evaluate the network
#print("[INFO] evaluating network...")
#predictions = model.predict(x=X_test, batch_size=32)
#print(classification_report(y_test.argmax(axis=1),
#	predictions.argmax(axis=1), target_names=lb.classes_))

# Guardar el Modelo
model.save('my_model.h5')
# plot the training loss and accuracy
N = np.arange(0, EPOCHS)
plt.style.use("ggplot")
plt.figure()
plt.plot(N, H.history["loss"], label="train_loss")
plt.plot(N, H.history["val_loss"], label="val_loss")
plt.plot(N, H.history["accuracy"], label="train_acc")
plt.plot(N, H.history["val_accuracy"], label="val_acc")
plt.title("Training Loss and Accuracy (Simple NN)")
plt.xlabel("Epoch #")
plt.ylabel("Loss/Accuracy")
plt.legend()
plt.show()

