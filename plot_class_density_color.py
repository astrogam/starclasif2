import matplotlib.pyplot as plt
from matplotlib.colors import rgb2hex
import csv
import sys
import math
from scipy.stats import gaussian_kde
import numpy as np
from sklearn.neighbors import KernelDensity

# This script plots predicted classes and Gaia effective temperature estimation.
# The color of the dots is function of density of stars in that range
# The brighter, the more stars were predicted as that class and have similar Gaia effective temperature.

classes = ['B','A','F','G','K','M']

# Get the color map
step = 1/100
yticks = [0 + step/2]
for i in range(100-1):
    yticks.append(yticks[-1]+step)

cmap = plt.cm.coolwarm
cmap = plt.cm.copper
my_colors = []
for i in yticks:
    rgba = cmap(round(i*cmap.N))
    my_colors.append(rgb2hex(rgba))

dataset = []
with open("predictions.csv","r") as fp:
    for s in csv.DictReader(fp):
        # If not a header
        if s['solution_id'] != 'solution_id':
            if s['lum_val'] == '':
                s['lum_val'] = 1
            else:
                s['lum_val'] = float(s['lum_val'])
            # Change > and < in if, to check how classifier works for brither or fainter stars
            # Uncomment if True, for all stars in the chart
            if float(s['phot_g_mean_mag']) > 13:
            #if True:
                dataset.append(s)

#classifiers = ['Nearest Neighbors','Linear SVM','RBF SVM', 'Gaussian Process','Random Forest','Neural Net']
classifiers = ['Linear SVM', 'Neural Net']
N = len(dataset)
for c in classifiers:
    set_order = plt.scatter(classes,[0,0,0,0,0,0])
    set_order.remove()
    teff_vals = {}
    for cl in classes:
        teff_vals[cl] = []
    for s in dataset:
        if s['teff_val'] != '' and s[c] != '':
            teff_vals[s[c]].append(s['teff_val'])
    kdes = {}
    probabilities = {}
    sample_bins = np.linspace(0,10000,101)
    sample_bins = np.array(sample_bins)
    sample_bins = sample_bins.reshape((len(sample_bins), 1))
    
    for cl in classes:
        try:
            teff_vals[cl]= np.array(teff_vals[cl]).reshape(-1,1)
            kdes[cl] = KernelDensity(kernel='gaussian', bandwidth=100).fit(teff_vals[cl])
            log_probabilities = kdes[cl].score_samples(sample_bins)
            probabilities[cl] = np.exp(log_probabilities)
            probabilities[cl] *= 100/max(probabilities[cl])
        except:
            continue

    for s in dataset:
        try:
            my_class = s[c]
            plt.scatter(s[c], float(s['teff_val']), c=my_colors[round(probabilities[my_class][round(float(s['teff_val'])/100)])], s=50)
        except:
            continue

    # Change < and > in title
    plt.title(str(N) + " stars from 49 Gulliver OCs with G > 13")
    plt.xlabel(c)
    plt.ylabel("Gaia effective temperature estimation (K)")
    plt.xticks(classes)
    plt.ylim(3000,10000)
    plt.yticks([3000,4000,5000,6000,7000,8000,9000,10000])
    plt.show()
