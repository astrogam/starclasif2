from astroquery.simbad import Simbad
import sys
import csv
from pprint import pprint

MySimbad = Simbad()
MySimbad.ROW_LIMIT = 1000 # now any query fetches at most 15 rows
MySimbad.TIMEOUT = 60 # sets the timeout to 20s

MySimbad.reset_votable_fields()
MySimbad.remove_votable_fields('coordinates')
MySimbad.add_votable_fields('otype', 'ra(dec,ICRS,J2000,2000)', 'dec(dec,ICRS,J2000,2000)', 'sptype', 'flux(B)', \
                            'flux(V)', 'flux(J)', 'flux(H)', 'flux(K)')
VOTfields= MySimbad.get_votable_fields()
pprint(VOTfields)

script = ("otype = 'Star' & spstring ~ 'B*' & Gmag < 18 & Bmag < 18 & Vmag < 18 & Jmag < 18 & Hmag < 18 & Kmag < 18")
result = MySimbad.query_criteria(script)

