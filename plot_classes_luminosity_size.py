import matplotlib.pyplot as plt
from matplotlib.colors import rgb2hex
import csv
import sys
import math
from scipy.stats import gaussian_kde
import numpy as np
from sklearn.neighbors import KernelDensity

# This script plots the predicted classes and Gaia effective temperature estimation
# The dot size is function of star luminosity

classes = ['B','A','F','G','K','M']

# Get the color map
step = 1/(len(classes))
yticks = [0 + step/2]
for i in range(len(classes)-1):
    yticks.append(yticks[-1]+step)

cmap = plt.cm.coolwarm
my_colors = []
for i in yticks:
    rgba = cmap(round(i*cmap.N))
    my_colors.append(rgb2hex(rgba))

dataset = []
with open("predictions.csv","r") as fp:
    for s in csv.DictReader(fp):
        # If not a header
        if s['solution_id'] != 'solution_id':
            if s['lum_val'] == '':
                s['lum_val'] = 1
            else:
                s['lum_val'] = float(s['lum_val'])
            dataset.append(s)

#classifiers = ['Nearest Neighbors','Linear SVM','RBF SVM', 'Gaussian Process','Random Forest','Neural Net']
classifiers = ['Linear SVM']
for c in classifiers:
    set_order = plt.scatter(classes,[0,0,0,0,0,0])
    set_order.remove()
    for s in dataset:
        try:
            plt.scatter(s[c], float(s['teff_val']), c=my_colors[classes.index(s[c])], s=s['lum_val'])
        except:
            continue

    plt.title("374 stars from 49 Gulliver OCs with G < 13")
    plt.xlabel(c)
    plt.ylabel("Gaia effective temperature estimation (K)")
    plt.xticks(classes)
    plt.ylim(3000,10000)
    plt.yticks([3000,4000,5000,6000,7000,8000,9000,10000])
    plt.show()
